#!/bin/bash
set -e
# ------------------------------------------------------------------------------
#     Rexsio agent installation script
# ------------------------------------------------------------------------------

REXSIO_AGENT_INSTALLER_RAW_REPO="https://gitlab.com/rexsio/agent-installer/raw/master"
REXSIO_AGENT_PIP_INSTALL_ARGS=""
REXSIO_AGENT_PIP_VERSION="rexsio-agent==0.0.8"
REXSIO_AGENT_AUTH_URL="https://api.rexs.io/v1/auth/token"
REXSIO_HOST="api.rexs.io"
REXSIO_WS_HOST="ws.rexs.io"
DOCKER_REGISTRY_LOGIN="app_rexsio"
DOCKER_REGISTRY_PASS="DgVA_MMzz2qSfNj5Wyyy"
DOCKER_REGISTRY_URL="registry.gitlab.com"

if [ "$1" == "--experimental" ]; then
    echo "============ Experimental mode ============"
    REXSIO_AGENT_INSTALLER_RAW_REPO="https://gitlab.com/rexsio/agent-installer/raw/experimental"
    REXSIO_AGENT_PIP_INSTALL_ARGS="--extra-index-url https://nexus.prod.dac.systems/repository/pip-public/simple"
    REXSIO_AGENT_PIP_VERSION="rexsio-agent"
    REXSIO_AGENT_AUTH_URL="https://api-dev.rexs.io/v1/auth/token"
    REXSIO_HOST="api-dev.rexs.io"
    REXSIO_WS_HOST="ws-service.rexsio.dev.dac.systems"
fi

# ------------------------------------------------------------------------------
# HELPER FUNCTIONS
# ------------------------------------------------------------------------------
create_os_group() {
    _group=${1}
    groupadd ${_group}
}
create_os_user() {
    _group=${1}
    _user=${2}
    useradd --create-home \
            --comment "Rexsio data notary system user (rexs.io)" \
            --gid ${_group} ${_user}
    usermod -a -G docker ${_user}
}
install_system_dependencies(){
    apt-get update && \
    apt-get install -y python3-dev python3-pip python3-venv && \
    apt-get install -y gnupg2 pass
}
request_user_and_device_codes(){
    curl -X POST ${REXSIO_AGENT_AUTH_URL} \
        -H 'Content-Type: application/x-www-form-urlencoded' \
        -d 'response_type=device_code&client_id=agent' 2>/dev/null
}
parse_user_code(){
    python -c 'import json, sys; obj=json.load(sys.stdin); print obj["user_code"];'
}
parse_device_code(){
    python -c 'import json, sys; obj=json.load(sys.stdin); print obj["device_code"];'
}
request_token(){
    _device_code=${1}
    _public_key=${2}
    curl -X POST ${REXSIO_AGENT_AUTH_URL} \
        -H 'Content-Type: application/x-www-form-urlencoded' \
        -d "grant_type=device_code&client_id=agent&code=${_device_code}&public_key=${_public_key}" 2>/dev/null
}
parse_token(){
    python -c 'import json, sys; obj=json.load(sys.stdin); print obj["access_token"];' 2>/dev/null
}
generate_rabbit_pass(){
    python -c 'import uuid; print(uuid.uuid4().hex);' 2>/dev/null
}
generate_private_and_public_key(){
    _keys_dir=${1}
    _openssl_image="registry.gitlab.com/bettersolutions/open-source/openssl-image:1.814534541"
    docker pull ${_openssl_image}
    docker run --rm -v ${_keys_dir}:/keys ${_openssl_image} genpkey -algorithm RSA -out keys/private_key.pem -pkeyopt rsa_keygen_bits:2048
    docker run --rm -v ${_keys_dir}:/keys ${_openssl_image} rsa -pubout -in keys/private_key.pem -out keys/public_key.pem
    docker rmi ${_openssl_image}
}
get_public_key(){
    _install_dir=${1}
    _keys_dir=${_install_dir}/config/keys
    generate_private_and_public_key ${_keys_dir} &>/dev/null
    cat ${_keys_dir}/public_key.pem | tr '+' '-' | tr '/' '_'
}
save_token_key_and_env_variables(){
    _install_dir=${1}
    _agent_dir=${2}
    _token=${3}
    _user=${4}
    keys_dir=${_install_dir}/config/keys
    token_filename=${_agent_dir}/access.token
    env_filename=${_agent_dir}/.env
    rabbit_pass=$(generate_rabbit_pass)
    echo "${_token}" > ${token_filename}
    echo "INSTALL_DIR=${_install_dir}" >> ${env_filename}
    echo "REXSIO_HOST=${REXSIO_HOST}" >> ${env_filename}
    echo "REXSIO_WS_HOST=${REXSIO_WS_HOST}" >> ${env_filename}
    echo "REXSIO_USER=${_user}" >> ${env_filename}
    echo "RABBITMQ_PASS=${rabbit_pass}" >> ${env_filename}
    mkdir ${keys_dir} --parents
    echo "" > ${keys_dir}/signature-key.txt
}
install_rexsio(){
    _agent_venv=${1}
    source ${_agent_venv}/bin/activate
    pip install --upgrade pip setuptools wheel
    pip install ${REXSIO_AGENT_PIP_INSTALL_ARGS} ${REXSIO_AGENT_PIP_VERSION}
    deactivate
}
login_to_docker_registry(){
    _user=${1}
    su ${_user} -c "docker login $DOCKER_REGISTRY_URL -u $DOCKER_REGISTRY_LOGIN -p $DOCKER_REGISTRY_PASS"
}
systemd_service_setup(){
    _dir=${1}
    _group=${2}
    _user=${3}
    _service_files_location="/etc/systemd/system"
    curl -fs \
        ${REXSIO_AGENT_INSTALLER_RAW_REPO}/rexsio.service | sed \
        -e "s#__REXSIO_AGENT_REPO_DIR__#${_dir}#g" \
        -e "s#__REXSIO_GROUP__#${_group}#g" \
        -e "s#__REXSIO_USER__#${_user}#g" > ${_service_files_location}/rexsio.service
}
create_uninstall_script(){
    _user=${1}
    _install_dir=${2}
    _uninstall_script_path="${_install_dir}/uninstall.sh"
    curl -fs ${REXSIO_AGENT_INSTALLER_RAW_REPO}/uninstall.sh > ${_uninstall_script_path}
    chmod 544 ${_uninstall_script_path}
    echo "${_user} ALL=(ALL) NOPASSWD: ${_uninstall_script_path}" >> /etc/sudoers
}
# ------------------------------------------------------------------------------
# PRE-REQUISITES AND RUNTIME VARIABLES
# ------------------------------------------------------------------------------
if [[ ${UID} -ne 0 ]]; then
    echo "ERROR: Execute this script as root or with sudo"
    exit 1
fi

if ! command -v python >/dev/null 2>&1; then
    echo "ERROR: python is missing"
    echo "try: sudo apt-get install python"
    exit 1
fi

if ! command -v docker >/dev/null 2>&1; then
    echo "ERROR: docker is missing"
    exit 1
fi

while true; do
    read -p "Choose group name [rexsio]: " group
    group=${group:-rexsio}
    if grep -q ${group} /etc/group >/dev/null 2>&1; then
        echo "ERROR: Group ${user} already exists. Choose a different name."
    else
        break
    fi
done
while true; do
    read -p "Choose user name [rexsio]: " user
    user=${user:-rexsio}
    if id -u ${user} >/dev/null 2>&1; then
        echo "ERROR: User ${user} already exists. Choose a different user name."
    else
        break
    fi
done
while true; do
    read -p "Choose installation directory [/opt/rexsio]: " install_dir
    install_dir=${install_dir:-"/opt/rexsio"}
    if [[ -d ${install_dir} ]]; then
        echo "ERROR: Directory ${install_dir} already exists. Select a differect location."
    else
        agent_dir=${install_dir}/agent
        agent_venv_dir=${agent_dir}/venv
        break
    fi
done
while true; do
    user_and_device_codes_response=$(request_user_and_device_codes)
    user_code=$(echo ${user_and_device_codes_response} | parse_user_code)
    device_code=$(echo ${user_and_device_codes_response} | parse_device_code)
    if [ -z "${user_code}" ] && [ -z "${device_code}" ]; then
        echo "ERROR: Permission denied."
        read -p "Press any key to continue... "$'\n' -n 1 -s
    else
        break
    fi
done

echo "Generating node keys. It may take a few seconds..."
public_key=$(get_public_key ${install_dir})

while true; do
    echo -e "Write the following code to the node creator:\n\n\e[1;32m${user_code} \e[0m\n"
    read -p "Then press any key to continue... "$'\n' -n 1 -s
    token_response=$(request_token ${device_code} "${public_key}")
    if token=$(echo ${token_response} | parse_token); then
        break
    else
        echo "ERROR: Wrong code."
    fi
done

# ------------------------------------------------------------------------------
# SETUP THE ENVIRONMENT
# ------------------------------------------------------------------------------
echo "Creating OS group..."; create_os_group ${group}
echo "Creating OS user..."; create_os_user ${group} ${user}
echo "Installing system dependencies..."; install_system_dependencies
echo "Creating installation directory..."; mkdir -p ${agent_dir}
echo "Authenticating with the Control Center..."; save_token_key_and_env_variables ${install_dir} ${agent_dir} ${token} ${user}
echo "Creating Rexsio Agent virtualenv..."; python3 -m venv ${agent_venv_dir}
echo "Installing Rexsio Agent..."; install_rexsio ${agent_venv_dir}
echo "Changing installation directory ownership..."; chown --recursive ${user}:${group} ${install_dir}
echo "Creating uninstall script..."; create_uninstall_script ${user} ${install_dir}
echo "Logging to docker registry..."; login_to_docker_registry ${user}
echo "Creating Rexsio Systemd service..."; systemd_service_setup ${agent_dir} ${group} ${user}
echo "Enabling Rexsio Systemd service..."; systemctl enable rexsio.service
echo "Starting Rexsio Systemd service..."; systemctl start rexsio.service
