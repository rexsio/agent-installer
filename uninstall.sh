#!/bin/bash
set -e
# ------------------------------------------------------------------------------
#     Rexsio agent uninstall script
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# HELPER FUNCTIONS
# ------------------------------------------------------------------------------
print_help_and_exit() {
    echo "Usage: uninstall.sh [rexsio installation path] [rexsio user]"
    exit 0
}

print_separator() {
    echo "============================"
}

shutdown_services() {
    _install_dir=${1}
    for service_path in ${_install_dir}/services/*; do
        if [ ${service_path} = "${_install_dir}/services/*" ]; then
            echo "No services found"
        else
            echo "Shutting down ${service_path} service and removing image..."
            docker-compose -f ${service_path}/docker-compose.yaml down -v --rmi local || echo "Service ${service_path} already removed"
        fi
    done
}

logout_from_docker_registry() {
    _user=${1}
    su ${_user} -c "docker logout"
}

remove_os_user() {
    _user=${1}
    userdel -f -r ${_user}
}
# ------------------------------------------------------------------------------
# PRE-REQUISITES AND RUNTIME VARIABLES
# ------------------------------------------------------------------------------
if [ "$1" == "-h" ]; then
    print_help_and_exit
fi
if [ -z "$1" ] || [ -z "$2" ]; then
    print_help_and_exit
fi

install_dir=${1}
user=${2}
# ------------------------------------------------------------------------------
# UNINSTALL
# ------------------------------------------------------------------------------
echo "Uninstalling Rexsio"
print_separator

echo "Shutting down services..."
shutdown_services ${install_dir}
print_separator

echo "Logging out from docker registry..."
logout_from_docker_registry ${user} || echo "Rexsio user is already loged out from docker registry"
print_separator

echo "Deleting ${user} OS user..."
remove_os_user ${user} || echo "${user} OS user is already removed"
print_separator

echo "Deleting ${install_dir}..."
rm -r ${install_dir} || echo "${install_dir} is already removed"
print_separator

echo "Disabling Rexsio Systemd service..."
systemctl disable rexsio.service || echo "Rexsio Systemd service is already disabled"
print_separator

echo "Removing Rexsio Systemd service..."
rm /etc/systemd/system/rexsio.service || echo "Rexsio Systemd service is already removed"
print_separator

echo "Stoping Rexsio Systemd service..."
systemctl stop rexsio.service || echo "Rexsio Systemd service is already stopped"
print_separator

echo "Rexsio Uninstalled"
