# REXS.IO Agent Installer

| :warning: You should not be installing REXS.IO Agent directly from this repository.|
| --- |

This git repository contains helper installation scripts and system template files required for provisionining a REXS.IO Agent on a new node.

## Usage
The `setup.sh` script in this repository will be executed during the installation procedure via the following call:

```shell
curl -fs -o setup.sh https://gitlab.com/rexsio/agent-installer/raw/master/setup.sh && sudo bash ./setup.sh
```
[REXS.IO's Web UI](https://app.rexs.io/config/pools) will actively prompt you to do so at the right point in time.